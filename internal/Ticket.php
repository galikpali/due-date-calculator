<?php

namespace IssueTrackerPro;

use DateTime;

class Ticket
{
    private DateTime $submissionDateTime;

    private int $turnaroundTime;

    public function __construct(DateTime $submissionDateTime, int $turnaroundTime)
    {
        $this->submissionDateTime = $submissionDateTime;
        $this->turnaroundTime = $turnaroundTime;
    }

    public function getSubmissionDate(): DateTime
    {
        return $this->submissionDateTime;
    }

    public function getTurnaroundTime(): int
    {
        return $this->turnaroundTime;
    }
}
