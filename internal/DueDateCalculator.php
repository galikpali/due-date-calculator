<?php

namespace IssueTrackerPro;

use DateTime;
use IssueTrackerPro\Exception\InvalidSubmissionDateTime;

class DueDateCalculator
{
    private const WORKING_HOURS_START = 9;
    private const WORKING_HOURS_STOP = 17;

    private const SATURDAY = 6;
    private const SUNDAY = 7;

    /**
     * @param Ticket $ticket
     * @return DateTime
     * @throws InvalidSubmissionDateTime
     */
    public function calculateDueDate(Ticket $ticket): DateTime
    {
        $this->validateTicket($ticket);

        $turnaroundTime = $ticket->getTurnaroundTime();
        $resolvedAt = $ticket->getSubmissionDate();

        while ($turnaroundTime !== 0) {
            if ($this->isBusinessHour($resolvedAt)) {
                $turnaroundTime--;
            }

            $resolvedAt->modify('+ 1 hour');
        }

        return $resolvedAt;
    }

    private function isBusinessHour(DateTime $resolvedAt): bool
    {
        return $this->isWorkingHour($resolvedAt) && !$this->isWeekend($resolvedAt);
    }

    private function isWorkingHour(DateTime $resolvedAt): bool
    {
        $hour = (int)$resolvedAt->format('G');

        return $hour >= self::WORKING_HOURS_START && $hour < self::WORKING_HOURS_STOP;
    }

    private function isWeekend(DateTime $resolvedAt): bool
    {
        $day = (int)$resolvedAt->format('N');

        return $day === self::SATURDAY || $day === self::SUNDAY;
    }

    /**
     * @param Ticket $ticket
     * @throws InvalidSubmissionDateTime
     */
    private function validateTicket(Ticket $ticket): void
    {
        if (!$this->isBusinessHour($ticket->getSubmissionDate())) {
            throw new InvalidSubmissionDateTime($ticket->getSubmissionDate()->format('d/m/Y g:i A'));
        }
    }
}
