<?php

namespace IssueTrackerPro;

use DateTime;
use Exception;
use PHPUnit\Framework\TestCase;

class TicketTest extends TestCase
{
    /**
     * @throws Exception
     */
    public function testGetters(): void
    {
        $submissionDateTime = new DateTime();
        $turnaroundTime = 5;
        $ticket = new Ticket($submissionDateTime, $turnaroundTime);

        $this->assertEquals($submissionDateTime, $ticket->getSubmissionDate());
        $this->assertEquals($turnaroundTime, $ticket->getTurnaroundTime());
    }
}
