<?php

namespace IssueTrackerPro;

use DateTime;
use Exception;
use IssueTrackerPro\Exception\InvalidSubmissionDateTime;
use PHPUnit\Framework\TestCase;

class DueDateCalculatorTest extends TestCase
{
    private DueDateCalculator $calculator;

    protected function setUp(): void
    {
        $this->calculator = new DueDateCalculator();
    }

    /**
     * @param Ticket $ticket
     * @param DateTime $expectedDateTime
     * @throws Exception
     *
     * @dataProvider dataProviderCalculateDueDate
     */
    public function testCalculateDueDate(Ticket $ticket, DateTime $expectedDateTime): void
    {
        $this->assertEquals(
            $this->getFormattedDate($expectedDateTime),
            $this->getFormattedDate($this->calculator->calculateDueDate($ticket))
        );
    }

    /**
     * @param Ticket $ticket
     * @throws InvalidSubmissionDateTime
     *
     * * @dataProvider dataProviderCalculateDueDateWhenTicketSubmittedAtNonBusinessHour
     */
    public function testCalculateDueDateWhenTicketSubmittedAtNonBusinessHour(Ticket $ticket): void
    {
        $this->expectException(InvalidSubmissionDateTime::class);
        $this->expectExceptionMessage($this->getFormattedDate($ticket->getSubmissionDate()));

        $this->calculator->calculateDueDate($ticket);
    }

    /**
     * @return array
     * @throws Exception
     */
    public function dataProviderCalculateDueDate(): array
    {
        return [
            "The issue is reported at Monday 9:00 AM, and it takes 1 hour to resolve,
             then the issue will be resolved at Monday 10:00 AM" => [
                $this->createTicketStub(new DateTime('this week Monday 9:00 AM'), 1),
                new DateTime('this week Monday 10:00 AM'),
            ],
            "The issue is reported at Monday 9:00 AM, and it takes 12 hours to resolve,
            then the issue will be resolved at next day 1:00 PM" => [
                $this->createTicketStub(new DateTime('this week Monday 9:00 AM'), 12),
                new DateTime('this week Tuesday 1:00 PM'),
            ],
            "The issue is reported at Monday 4:59 PM, and it takes 2 hours to resolve,
            then the issue will be resolved at next day 10:59 AM" => [
                $this->createTicketStub(new DateTime('this week Monday 4:59 PM'), 2),
                new DateTime('this week Tuesday 10:59 AM'),
            ],
            "The issue is reported at Friday 2:00 PM, and it takes 9 hours to resolve,
            then the issue will be resolved at next Monday 3:00 PM" => [
                $this->createTicketStub(new DateTime('this week Friday 2:00 PM'), 9),
                new DateTime('next week Monday 3:00 PM'),
            ],
            "The issue is reported at Tuesday 2:12 PM and it takes 16 hours to resolve,
            then the issue will be resolved on Thursday 2:12 PM" => [
                $this->createTicketStub(new DateTime('this week Tuesday 2:12 PM'), 16),
                new DateTime('this week Thursday 2:12 PM'),
            ],
        ];
    }

    public function dataProviderCalculateDueDateWhenTicketSubmittedAtNonBusinessHour(): array
    {
        return [
            'The issue reported on Monday 8:59 AM, just before working hours started' => [
                $this->createTicketStub(new DateTime('Monday 8:59 AM'), 1),
            ],
            'The issue reported on Monday 5:00 AM, just after working hours ended' => [
                $this->createTicketStub(new DateTime('Monday 5:00 PM'), 1),
            ],
            'The issue is reported on Saturday' => [
                $this->createTicketStub(new DateTime('Saturday 10:00 AM'), 1),
            ],
            'The issue is reported on Sunday' => [
                $this->createTicketStub(new DateTime('Sunday 10:00 AM'), 1),
            ],
        ];
    }

    private function createTicketStub(DateTime $submissionDateTime, int $turnaroundTime): Ticket
    {
        $ticketStub = $this->createMock(Ticket::class);
        $ticketStub
            ->method('getSubmissionDate')
            ->willReturn($submissionDateTime);
        $ticketStub
            ->method('getTurnaroundTime')
            ->willReturn($turnaroundTime);

        return $ticketStub;
    }

    private function getFormattedDate(DateTime $dateTime): string
    {
        return $dateTime->format('d/m/Y g:i A');
    }
}
